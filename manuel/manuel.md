# Manuel d'uilisation du système CSB BoR

### Install

* [On installe le **système** de jeu](#on-installe-le-système-de-jeu)
* [On installe le **module** BoR](#on-installe-le-module-bor)
* [On crée un monde sous système Custom System Builder](#on-crée-un-monde-sous-système-custom-system-builder)
* [On le démarre](#on-le-démarre)
* [On active le module BoR](#on-active-le-module-bor)
* [On **importe** les modèles et les acteurs, objets,...](#on-importe-les-modèles-et-les-acteurs-objets)
* [Et voilà !](#et-voilà-)

### Création de perso

* [On crée un **personnage** (joueur)](#on-crée-un-personnage-joueur)
* [On passe en mode "**édition**" dans l'onglet Equipement/Historique](#on-passe-en-mode-édition-dans-longlet-equipementhistorique)
* [On s'ajoute une carrière à partir des objets/Carrière par glisser-déposer sur la fiche](#on-sajoute-une-carrière-à-partir-des-objetscarrière-par-glisser-déposer-sur-la-fiche)
* [On se donne du matos à partir des objets par glisser-déposer sur la fiche](#on-se-donne-du-matos-à-partir-des-objets-par-glisser-déposer-sur-la-fiche)
* [On sait parler des langues (important pour lire les ouvrages)](#on-sait-parler-des-langues-important-pour-lire-les-ouvrages)
* [On s'ajoute des avantages/désavantages à partir des objets par glisser-déposer sur la fiche](#on-sajoute-des-avantagesdésavantages-à-partir-des-objets-par-glisser-déposer-sur-la-fiche)
* [On teste en faisant un jet d'attribut](#on-teste-en-faisant-un-jet-dattribut)
* [On utilise la Chance](#on-utilise-la-chance)
* [On crée un **personnage** (non-joueur)](#on-crée-un-personnage-non-joueur)
* [On choisit sa puissance](#on-choisit-sa-puissance)
* [On le crée](#on-le-crée)

### Ca va friter !

* [On importe la macro d'activation du combat (dégâts automatiques) par glisser-déposer dans la barre de macros](#on-importe-la-macro-dactivation-du-combat-dégâts-automatiques-par-glisser-déposer-dans-la-barre-de-macros)
* [On passe au **combat** (avec les tokens)](#on-passe-au-combat-avec-les-tokens)
* [On active le mode combat: dégâts automatiques](#on-active-le-mode-combat-dégâts-automatiques)
* [On l'attaque après l'avoir désigné comme cible dans la scène (touche T)](#on-lattaque-après-lavoir-désigné-comme-cible-dans-la-scène-touche-t)
* [On lui fait des dégâts](#on-lui-fait-des-dégâts)

## On installe le système de jeu

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0010_installsys01.jpg)

01

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0020_installsys02.jpg)

02

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0030_installsys03.jpg)

03

## On installe le module BoR

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0040_installmod01.jpg)

04

## On crée un monde sous système Custom System Builder

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0050_creemond01.jpg)

05

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0060_creemond02.jpg)

06

## On le démarre

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0070_demarre01.jpg)

07

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0080_demarre02.jpg)

08

## On active le module BoR

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0090_activemod01.jpg)

09

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0100_activemod02.jpg)

10

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0110_activemod03.jpg)

11

## On importe les modèles et les acteurs, objets,...

⚠️ SURTOUT bien dans l'ORDRE: d'abord les OBJETS puis les ACTEURS ! ⚠️

⚠️ Bien cocher **Garder l'ID**

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0120_importcomp01.jpg)

12

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0130_importcomp02.jpg)

13

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0140_importcomp03.jpg)

14

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0150_importcomp04.jpg)

15

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0160_importcomp05.jpg)

16

## Et voilà !

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0170_etvoila01.jpg)

17

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0180_etvoila02.jpg)

18

## On crée un personnage (joueur)

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0190_creepersoj01.jpg)

19

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0200_creepersoj02.jpg)

20

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0210_creepersoj03.jpg)

21

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0220_creepersoj04.jpg)

22

## On passe en mode "édition" dans l'onglet Equipement/Historique

On alloue des points aux attributs, on met les jaudes au max.

On remplit les détails.

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0230_modeedit01.jpg)

23

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0240_modeedit02.jpg)

24

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0250_modeedit03.jpg)

25

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0260_modeedit04.jpg)

26

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0270_modeedit05.jpg)

27

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0280_modeedit06.jpg)

28

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0290_modeedit07.jpg)

29

## On s'ajoute une carrière à partir des objets/Carrière par glisser-déposer sur la fiche

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0300_ajoutcarr01.jpg)

30

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0310_ajoutcarr02.jpg)

31

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0320_ajoutcarr03.jpg)

32

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0330_ajoutcarr04.jpg)

33

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0340_ajoutcarr05.jpg)

34

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0350_ajoutcarr06.jpg)

35

## On se donne du matos à partir des objets par glisser-déposer sur la fiche

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0360_donnmatos01.jpg)

36

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0370_donnmatos02.jpg)

37

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0380_donnmatos03.jpg)

38

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0390_donnmatos04.jpg)

39

On peut choisir une posture de combat.

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0400_donnmatos05.jpg)

40

## On sait parler des langues (important pour lire les ouvrages)

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0410_parlerlang01.jpg)

41

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0420_parlerlang02.jpg)

42

## On s'ajoute des avantages/désavantages à partir des objets par glisser-déposer sur la fiche

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0430_ajoutavant01.jpg)

43

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0440_ajoutavant02.jpg)

44

## On teste en faisant un jet d'attribut

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0450_testattrib01.jpg)

45

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0460_testattrib02.jpg)

46

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0470_testattrib03.jpg)

47

## On utilise la Chance

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0480_utilchance01.jpg)

48

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0490_utilchance02.jpg)

49

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0500_utilchance03.jpg)

50

## On crée un personnage (non-joueur)

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0510_creepersonj01.jpg)

51

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0520_creepersonj02.jpg)

52

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0530_creepersonj03.jpg)

53

## On choisit sa puissance

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0540_choipuis01.jpg)

54

## On le crée

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0550_creepnj01.jpg)

55

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0560_creepnj02.jpg)

56

## On importe la macro d'activation du combat (dégâts automatiques) par glisser-déposer dans la barre de macros

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0561_impmacroactiv01.jpg)

56.1

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0562_impmacroactiv02.jpg)

56.2

## On passe au combat (avec les tokens)

⚠️ Il faut avoir créé un joueur au minimum sur votre partie pour que cela fonctionne.

On vous conseille le module Ownership Viewer https://foundryvtt.com/packages/permission_viewer pour plus de lisibilité à l'écran.

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0563_passcomb01.jpg)

56.3

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0564_passcomb02.jpg)

56.4

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0565_passcomb03.jpg)

56.5

## On active le mode combat: dégâts automatiques

Click sur la macro précédemment importée.

Message dans le chat pour chaque combattant adverse activé (en combat).

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0566_activcomb01.jpg)

56.6

## On l'attaque après l'avoir désigné comme cible dans la scène (touche T)

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0570_attaq01.jpg)

57

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0580_attaq02.jpg)

58

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0590_attaq03.jpg)

59

## On lui fait des dégâts

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0600_degat01.jpg)

60

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0610_degat02.jpg)

61

![](https://gitlab.com/vonv/fvtt-csb-vonv-sys-bor/-/raw/main/manuel/manuel0620_degat03.jpg)

62

